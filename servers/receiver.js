const express = require("express");
const app = express();
const chalk = require("chalk");

const {Rabbitmq} = require('../connections/Rabbitmq');
const {testQueue} = require('../controller/testQueue');
const {connectingToQueues} = require('../app/helper');
const {route} = require('../routers/routes');

app.use(express.json());

const PORT = process.env.PORT || 4002;

connectQueue()
async function connectQueue() {
    try {
        //* setup rabbitmq connection:
        const rabbitmq = new Rabbitmq("amqp://127.0.0.1:5672");
        const channel = await rabbitmq.createConnection();

        //* connecting to rabbitmq queues:
        connectingToQueues(channel);

        //* managing all queues:
        route(channel);

        //* starting of application:
        console.log(chalk.bold.cyan('App Is Run!'));

    } catch (error) {
        console.log(error)
    }
}

app.listen(PORT, () => console.log('\n',chalk.bold.green("Server running at port " + PORT),'\n'));