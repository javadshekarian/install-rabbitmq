const chalk = require('chalk');

const log = console.log;

var queues = [
    {
        queueName:'test-queue',
        explain:'this route is just for test'
    },{
        queueName:'queue1',
        explain:'this route is just for test 1'
    },{
        queueName:'queue2',
        explain:'this route is just for test 2'
    },{
        queueName:'queue3',
        explain:'this route is just for test 3'
    },{
        queueName:'queue4',
        explain:'this route is just for test 4'
    },{
        queueName:'queue5',
        explain:'this route is just for test 5'
    }
]

var promiseQueue = [];

module.exports.connectingToQueues = async (channel) => {
    queues.forEach(index=>{
        promiseQueue = [...promiseQueue,(async function(){
            await channel.assertQueue(index.queueName);
        })()]
    });

    console.log(chalk.bold.rgb(255,165,0)(`Join To All Queue:`))
    Promise.all(promiseQueue);
    console.log(chalk.bold.rgb(255,165,0)(`End Of Join`));
}