const chalk = require('chalk');
const controller = require('../controller/testQueue');

const route = (channel) => {
    console.log(chalk.bold.rgb(255,165,0)('\nSetup Controller'));

    //* start of routes:
    channel.consume('test-queue',data=>controller.testQueue(channel,data));
    //* end of routes

    console.log(chalk.bold.rgb(255,165,0)('End Of Setup Controller'),'\n');
}

module.exports.route = route;