const amqp = require("amqplib");

module.exports.Rabbitmq = class Rabbitmq {
    constructor(url){
        this.url = url;
    }

    async createConnection(){
        const connection = await amqp.connect(this.url);
        const channel = await connection.createChannel();
        return channel;
    }
}